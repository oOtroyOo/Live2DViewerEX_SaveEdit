package com.zy.tools;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.UriPermission;
import android.net.Uri;
import android.util.Log;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import androidx.documentfile.provider.DocumentFile;


public class Tool {


    Activity currentActivity;

    public String pkg;

    public Activity GetCurrentActivity() {
        return currentActivity;
    }

    public Context GetCurrentActivityContext() {
        return (Context) GetCurrentActivity();
    }

    public Tool(Activity activity) {
        this.currentActivity = activity;
    }

    public void RequestDataPermission(Runnable callback) {
        ToolFragment.callback = callback;
        boolean granted = false;
        for (UriPermission persistedUriPermission : currentActivity.getContentResolver().getPersistedUriPermissions()) {
            if (persistedUriPermission.isReadPermission() && persistedUriPermission.getUri().toString().contains("content://com.android.externalstorage.documents/tree/primary%3AAndroid%2Fdata")) {
                granted = true;
            }
        }
        if (granted) {
            callback.run();
        } else {
            final Fragment request = new ToolFragment();
            //Bundle bundle = new Bundle();
            //bundle.putString("pkg", pkg);
            //request.setArguments(bundle);
            ((Activity) currentActivity).getFragmentManager().beginTransaction().add(0, request).commit();
        }
    }

    //根据路径获得document文件
    public static DocumentFile getDoucmentFile(Context context, String path) {
        if (path.endsWith("/")) {
            path = path.substring(0, path.length() - 1);
        }
        return DocumentFile.fromSingleUri(context, Uri.parse(changeToUri(path)));
    }

    public String[] ListDir(String path) {
        //content://com.android.externalstorage.documents/tree/primary%3AAndroid%2Fdata/document/primary%3AAndroid%2Fdata%2Fcn.oneplus.photos
        DocumentFile doucmentDir = DocumentFile.fromTreeUri(currentActivity, Uri.parse(changeToUri(path)));
        //content://com.android.externalstorage.documents/tree/primary%3AAndroid%2Fdata/document/primary%3AAndroid%2Fdata%2Fcom.pavostudio.live2dviewerex%2Ffiles%2Fsave%2Fcha
        ArrayList<DocumentFile> list = new ArrayList<DocumentFile>();
        ListDocument(list, doucmentDir, path.replace("Android/data/", ""));
        String[] files = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            files[i] = UriToFile(list.get(i).getUri());
        }
        return files;
    }

    public void ListDocument(ArrayList<DocumentFile> list, DocumentFile documentFile, String head) {
        String splitHead = null;
        if (head != null && head.length() > 0)
            splitHead = head.split("/")[0];
        if (documentFile.isDirectory()) {
            try {
                for (DocumentFile file : documentFile.listFiles()) {
                    String uri = file.getUri().toString();
                    if (head != null && head.length() > 0 && !uri.endsWith(splitHead)) {
                        continue;
                    }

                    if (file.isFile()) {
                        list.add(file);
                    }
                    if (file.isDirectory()) {
                        if (splitHead == null) {
                            ListDocument(list, file,null);//递归调用
                        } else {
                            ListDocument(list, file, head.substring(splitHead.length() + 1));//递归调用

                        }
                    }
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public byte[] ReadFile(String path) {
        try {
            DocumentFile documentFile = getDoucmentFile(currentActivity, path);
            InputStream inputStream = currentActivity.getContentResolver().openInputStream(documentFile.getUri());
            byte[] data = new byte[inputStream.available()];
            inputStream.read(data);
            return data;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }

    public boolean WriteFile(String path, byte[] data) throws IOException {

        try {
            DocumentFile documentFile = getDoucmentFile(currentActivity, path);
            OutputStream outputStream = currentActivity.getContentResolver().openOutputStream(documentFile.getUri(), "rw");
            outputStream.write(data, 0, data.length);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    String UriToFile(Uri uri) {
        //content://com.android.externalstorage.documents/tree/primary%3AAndroid%2Fdata%2Fcom.pavostudio.live2dviewerex/document/primary%3AAndroid%2Fdata%2Fcom.pavostudio.live2dviewerex%2Ffiles
        String path = uri.toString();
        String search ="document/primary";
        int index =path.indexOf(search);
        path = path.substring(index + search.length() + 3);
        path = path.replace("%2F", "/");
        return path;
    }

    public static String changeToUri(String path) {
        if (path.endsWith("/")) {
            path = path.substring(0, path.length() - 1);
        }
        String path2 = path.replace("/storage/emulated/0/", "").replace("/", "%2F");
        return "content://com.android.externalstorage.documents/tree/primary%3AAndroid%2Fdata/document/primary%3A" + path2;
    }

    //转换至uriTree的路径
    public static String changeToUri3(String path) {
        path = path.replace("/storage/emulated/0/", "").replace("/", "%2F");
        return ("content://com.android.externalstorage.documents/tree/primary%3A" + path);

    }


    /**
     * 应用程序运行命令获取 Root权限，设备必须已破解(获得ROOT权限)
     *
     * @return 应用程序是/否获取Root权限
     */
    /**
     * 应用程序运行命令获取 Root权限，设备必须已破解(获得ROOT权限)
     *
     * @return 应用程序是/否获取Root权限
     */
    public boolean upgradeRootPermission() {

        //GetCurrentActivity().runOnUiThread(new Runnable() {
        //    @Override
        //    public void run() {
        //        RootCommand("cd /data/data ; ls -a");
        //    }
        //});
        return RootCommand("chmod 777 " + GetCurrentActivity().getPackageCodePath());
    }

    public boolean RootCommand(String command) {

        Process process = null;
        DataOutputStream os = null;
        try {
            process = Runtime.getRuntime().exec("su");
            os = new DataOutputStream(process.getOutputStream());
            os.writeBytes(command + "\n");
            os.writeBytes("exit\n");
            os.flush();
            process.waitFor();
        } catch (Exception e) {
            Log.d("*** DEBUG ***", "ROOT REE" + e.getMessage());
            return false;
        } finally {
            try {
                if (os != null) {
                    os.close();
                }
                process.destroy();
            } catch (Exception e) {
            }
        }
        Log.d("*** DEBUG ***", "Root SUC " + command);
        return true;
    }

}